package com.example.android.myinventoryapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.example.android.myinventoryapp.Data.InventoryDbHelper;
import com.example.android.myinventoryapp.Data.StockItem;


public class MainActivity extends AppCompatActivity {

    InventoryDbHelper dbHelper;
    StockCursorAdapter adapter;
    int lastVisibleItem = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = new InventoryDbHelper(this);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.addFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                startActivity(intent);
            }
        });

        final ListView listView = (ListView) findViewById(R.id.list_view);
        View emptyView = findViewById(R.id.empty_view);
        listView.setEmptyView(emptyView);

        Cursor cursor = dbHelper.readStock();
        adapter = new StockCursorAdapter(this, cursor);
        listView.setAdapter(adapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == 0) return;
                final int currentFirstVisibleItem = view.getFirstVisiblePosition();
                if (currentFirstVisibleItem > lastVisibleItem) {
                    fab.show();
                } else if (currentFirstVisibleItem < lastVisibleItem) {
                    fab.hide();
                }
                lastVisibleItem = currentFirstVisibleItem;
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.swapCursor(dbHelper.readStock());
    }

    public void clickOnViewItem(long id) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("itemId", id);
        startActivity(intent);
    }

    public void clickOnSale(long id, int quantity) {
        dbHelper.sellOneItem(id, quantity);
        adapter.swapCursor(dbHelper.readStock());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_dummy_data:
                addDummyData();
                adapter.swapCursor(dbHelper.readStock());
        }
        return super.onOptionsItemSelected(item);
    }


    private void addDummyData() {
        StockItem iphone = new StockItem(
                "iphone 7",
                "16299.00 EGP",
                4,
                "Fayez Store",
                "+2011100768484",
                "Fayez.store@gmail.com",
                "android.resource://com.example.android.myinventoryapp/drawable/iphone");
        dbHelper.insertItem(iphone);

        StockItem nexus = new StockItem(
                "ASUS Google Nexus 7",
                "1999.00 EGP",
                2,
                "Fayez Store",
                "+2011100768484",
                "Fayez.store@gmail.com",
                "android.resource://com.example.android.myinventoryapp/drawable/nexus");
        dbHelper.insertItem(nexus);

        StockItem samsung = new StockItem(
                "samsung galaxy s7",
                "9999.00 EGP",
                7,
                "Fayez Store",
                "+2011100768484",
                "Fayez.store@gmail.com",
                "android.resource://com.example.android.myinventoryapp/drawable/samsung");
        dbHelper.insertItem(samsung);

        StockItem sony = new StockItem(
                "Sony Xperia XZ",
                "9999.00 EGP",
                3,
                "Fayez Store",
                "+2011100768484",
                "Fayez.store@gmail.com",
                "android.resource://com.example.android.myinventoryapp/drawable/sony");
        dbHelper.insertItem(sony);
    }
}